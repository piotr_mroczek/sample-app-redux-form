import React from 'react';
import ReactDOM from 'react-dom';
import { Redux, compose } from 'redux';

import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Component } from 'react';
import { Provider } from 'react-redux';
import { reducer as formReducer } from 'redux-form'

var $ = require('jquery-browserify');

import ContactForm from  './ContactForm';
import ContactFormAsJson from  './ContactFormAsJson';


const contactApp = combineReducers({
    form: formReducer     // <---- Mounted at 'form'
});


let store = createStore(contactApp, window.devToolsExtension && window.devToolsExtension());

// let store = createStore(contactApp, compose(
//     applyMiddleware(thunk),
//     window.devToolsExtension ? window.devToolsExtension() : f => f
// ));


var handlerSubmit= (e,a,b) => {
    b.preventDefault()
    console.log('e', e);
    console.log('e.target', e.target.form);
    console.log('a', a);
    console.log('b', b);

    window.alert(`You submitted:\n\n${JSON.stringify(e, null, 2)}`)
    e.preventDefault()
}

const showResults = values =>
    new Promise(resolve => {
        setTimeout(() => {  // simulate server latency
        window.alert(`You submitted:\n\n${JSON.stringify(values, null, 2)}`)
        resolve()
    }, 500)
})

const showResultsNow = values =>{
    window.alert(`You submitted:\n\n${JSON.stringify(values, null, 2)}`)

}


//onSubmit={showResults}


var placeholder = $('#app');
const render = () => {

    // console.log("[RENDER] ",  {...store.getState() }  );

    ReactDOM.render(
        <Provider store={store}>

            <div className="row">

                <div className="col-md-6">
                    <ContactForm onSubmit={showResultsNow}
                    />
                </div>

                <div className="col-md-6">

                    <ContactFormAsJson    />

                </div>
            </div>


        </Provider>
            ,
        placeholder[0]
    );

};

// store.subscribe(render);
render();

