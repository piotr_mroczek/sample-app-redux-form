import React, { Component } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

class ContactFormAsJson extends Component {
    render() {

        // // const { form } = this.props;
        var stateAsJson  = JSON.stringify( this.props.state.form.contact.values );

        console.log('stateAsJson', stateAsJson)


        return (

            <div>
                <span>Reprezentacja danych jako JSON</span>
                <textarea  className="form-control" rows="10" value={stateAsJson} />

            </div>


    );
    }
}

/**
 * This function maps the state to a
 * prop called `state`.
 *
 * In larger apps it is often good
 * to be more selective and only
 * map the part of the state tree
 * that is necessary.
 */
const mapStateToProps = (state) => ({
    state: state
});

/**
 * This function maps actions to props
 * and binds them so they can be called
 * directly.
 *
 * In this case all actions are mapped
 * to the `actions` prop.
 */
// const mapDispatchToProps = (dispatch) => ({
//     actions: bindActionCreators(Actions, dispatch)
// })

const mapDispatchToProps = (dispatch) => {
    return {
            onTodoClick: (id) => {
            console.log(id);
        }
    }
}


/**
 * Finally the Redux store is connected
 * to the component with the `connect()`
 * function.
 */
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ContactFormAsJson);


// export default ContactFormAsJson;

