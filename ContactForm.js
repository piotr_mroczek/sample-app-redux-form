import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';

class ContactForm extends Component {
    render() {
        const { handleSubmit } = this.props;
        console.log('handleSubmit', handleSubmit);

        return (

            <div className="panel panel-primary" id="event-details">

                <div className="panel-heading">Formularz kontaktowy</div>

                <div className="panel-body">

                <form onSubmit={handleSubmit} noValidate="novalidate">

                        <div className="form-group">
                            <label htmlFor="firstName">Imię</label>
                            <Field name="firstName" component="input" type="text"  className="form-control" />
                        </div>

                        <div className="form-group">
                        <label htmlFor="lastName">Nazwisko</label>
                             <Field name="lastName" component="input" type="text"  className="form-control" />
                        </div>

                        <div className="form-group">
                            <label htmlFor="email">Email</label>
                            <Field name="email" component="input" type="email"   className="form-control" />
                        </div>

                        <button type="submit" className="btn btn-primary">Zapisz</button>

                </form>

                </div>
            </div>

    );
    }
}

// Decorate the form component
ContactForm = reduxForm({
    form: 'contact' // a unique name for this form
})(ContactForm);

export default ContactForm;

